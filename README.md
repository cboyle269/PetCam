# Pet Cam
This is a simple Node.JS server that provides a web app to view the video stream from a raspberry pi running motion. See [here](https://motion-project.github.io) for more information on motion. 

## Commands 
### `npm run start`
This will build the client code and start the express server on localhost port 5000
### `npm run CreateUser`
This runs a console app that will prompt you for an encryption key and then a series of usernames and passwords. The passwords are then encyrpted using the key and saved written to a file. This encryption key must be provided either in the keys_dev.js file or passed in as an evironment variable at runtime. This is so the credentials used for the live app are never checked into version control. 
### `npm run encryptStreamPassword`
Simmilar to the previous command this will prompt you for an encryption key and then a password. The password is the password needed for the basic auth header when connection to motion on the raspberry pi. This encryption key provided must be the same as that used for the users. 

## Rest API
This server uses express to expose two rest endpoints 
### `api/login`
As the name suggests takes a username and password and returns a 200 if the authentication is successfull or returns a 400 with a list of errors if authentication is unsuccessful. 
### `api/stream`
This endpoint is a simple proxy to the stream from the raspberry pi. The only notable thing this does is add a basic auth header to the request to the raspberry pi. 




