const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const login = require('./routes/login');
const stream = require('./routes/stream');

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(express.static('./client/build'));

const staticpath = path.resolve(
  __dirname,
  '..',
  'client',
  'build',
  'index.html'
);

console.log(staticpath);

app.use('/', login);
app.use('/', stream);

app.get('*', (req, res) => {
  res.sendFile(path.resolve);
});

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server is lisenting on port [${port}]`));
