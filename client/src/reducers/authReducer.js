import { SET_CURRENT_USER } from '../actions/types';

const initialState = {
  isAuthenticated: false,
  user: ''
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_CURRENT_USER:
      if (action.payload === null) {
        return {
          ...state,
          isAuthenticated: false,
          user: ''
        };
      } else {
        return {
          ...state,
          isAuthenticated: true,
          user: action.payload
        };
      }
    default:
      return state;
  }
}
