import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Header from './Header';
import Footer from './Footer';
import classnames from 'classnames';

export class MainApp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      camId: 1
    };
  }

  onButtonClick = id => {
    console.log(`setting camera id to : ${id}`);
    this.setState({
      camId: id
    });
  };

  render() {
    const imgSrc = `/api/stream/${this.state.camId}`;

    return (
      <div>
        <Header history={this.props.history} />
        <div className="container mb-2">
          <div className="row">
            <div className="col text-center">
              <button
                name="cam1-btn"
                onClick={e => {
                  e.preventDefault();
                  this.onButtonClick(1);
                }}
                className={classnames('btn', {
                  'btn-outline-success': this.state.camId !== 1,
                  'btn-success': this.state.camId === 1
                })}
              >
                {' '}
                Camera 1{' '}
              </button>
            </div>
            <div className="col text-center">
              <button
                name="cam2-btn"
                onClick={e => {
                  e.preventDefault();
                  this.onButtonClick(2);
                  console.log(this.state.camId);
                }}
                className={classnames('btn', {
                  'btn-outline-success': this.state.camId !== 2,
                  'btn-success': this.state.camId === 2
                })}
              >
                {' '}
                Camera 2{' '}
              </button>
            </div>
          </div>
        </div>
        <div className="container">
          <img
            title="Stream"
            className="img-fluid rounded"
            src={imgSrc}
            alt="oops..."
          />
        </div>

        <Footer />
      </div>
    );
  }

  componentDidMount() {
    if (!this.props.auth.isAuthenticated) {
      this.props.history.push('/login');
    }
  }
}

MainApp.propTypes = {
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  {}
)(MainApp);
