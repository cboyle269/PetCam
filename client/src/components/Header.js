import React, { Component } from 'react';
import { connect } from 'react-redux';
import { logOutUser } from '../actions/authActions';

export class Header extends Component {
  onClick = () => {
    console.log(`Outside action ${this.props.history}`);
    this.props.logOutUser(this.props.history);
  };

  render() {
    return (
      <nav className="navbar navbar-dark bg-success mb-3">
        <span className="navbar-brand">Peppit Cam</span>

        <button onClick={this.onClick} className="btn nav-btn  btn-danger">
          Logout
        </button>
      </nav>
    );
  }
}

export default connect(
  null,
  { logOutUser }
)(Header);
