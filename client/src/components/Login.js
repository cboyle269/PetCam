import React, { Component } from 'react';
import { connect } from 'react-redux';
import { loginUser, clearErrors } from '../actions/authActions';
import classnames from 'classnames';

export class Login extends Component {
  constructor() {
    super();
    this.state = {
      username: '',
      password: '',
      errors: {}
    };
  }

  onChange = e => {
    const fieldName = e.target.name;
    const input = e.target.value;
    this.setState({
      [fieldName]: input
    });
  };

  onSubmit = e => {
    e.preventDefault();
    this.props.loginUser(this.state, this.props.history);
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.errors !== prevState.errors) {
      return { errors: nextProps.errors };
    }
    if (nextProps.auth.isAuthenticated) {
      nextProps.history.push('/');
    }
    return null;
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.errors !== prevState.errors) {
      this.setState({ errors: prevProps.errors });
    }
  }

  componentDidMount() {
    if (this.props.auth.isAuthenticated) {
      this.props.history.push('/');
    }
    this.props.clearErrors();
  }

  render() {
    console.log(this.state.errors.username);
    return (
      <div className="row">
        <div className="card login-card mt-3 align-center mx-auto">
          <img
            src="/login-image.gif"
            alt="Login-img"
            className="card-img-top login-img"
          />
          <div className="card-body">
            <h2 className="card-title text-center">Login</h2>
            <form onSubmit={this.onSubmit}>
              <div className="form-group">
                <label htmlFor="username">Username</label>
                <input
                  type="text"
                  className={classnames('form-control', {
                    'is-invalid': this.state.errors.username
                  })}
                  id="username"
                  name="username"
                  placeholder="Enter username"
                  value={this.state.username}
                  onChange={this.onChange}
                />
                {this.state.errors.username && (
                  <div className="invalid-feedback">
                    {this.state.errors.username}
                  </div>
                )}
              </div>
              <div className="form-group">
                <label htmlFor="password">Password</label>
                <input
                  type="Password"
                  className={classnames('form-control', {
                    'is-invalid': this.state.errors.password
                  })}
                  id="password"
                  name="password"
                  value={this.state.password}
                  onChange={this.onChange}
                />
                {this.state.errors.password && (
                  <div className="invalid-feedback">
                    {this.state.errors.password}
                  </div>
                )}
              </div>

              <button className="btn btn-success mx-auto">Submit</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  errors: state.errors,
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { loginUser, clearErrors }
)(Login);
