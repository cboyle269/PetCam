import React from 'react';

export default function Footer() {
  return (
    <nav className="nav bg-dark footer mt-5">
      <div className="container">
        <p className="text-light text-center">Created by Connor Boyle with ❤</p>
      </div>
    </nav>
  );
}
