import axios from 'axios'

import { SET_CURRENT_USER } from './types';
import { CLEAR_ERRORS } from './types';
import { GET_ERRORS } from './types';

export const loginUser = (userData, history) => dispatch => {
  axios.post('/api/login', userData)
    .then(res => {
      if(res.data.success){
        dispatch({
          type: SET_CURRENT_USER,
          payload: userData.username
        })
      }
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS, 
        payload: err.response.data
      })
    })
};

export const logOutUser = history => dispatch => {
  console.log(`instide action ${history}`);
  dispatch({
    type: SET_CURRENT_USER,
    payload: null
  });
  history.push('/login');
};

export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};
