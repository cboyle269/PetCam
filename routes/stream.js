const express = require('express');
const router = express.Router();
const request = require('request');
const encryptionHelpers = require('../util/encryptionHelpers');

const streamPassword = encryptionHelpers.decryptStreamPassword();

//@ route GET /api/stream
router.get('/api/stream/:id', (req, res) => {
  const camId = req.params.id;
  try {
    req
      .pipe(
        request({
          url: `https://cboyleraspi1.hopto.org:8081/${camId}`,
          agentOptions: {
            // Required since raspberry pi is using self signed cert
            rejectUnauthorized: false
          }
        }).auth('cboyle', streamPassword)
      )
      .pipe(res);
  } catch (e) {
    res.status(500);
  }
});

module.exports = router;
