const express = require('express');
const router = express.Router();

const fileHelpers = require('../util/fileHelpers');
const encryptionHelpers = require('../util/encryptionHelpers');

//@route POST /api/login
router.post('/api/login', (req, res) => {
  let errors = {};
  const { username, password } = req.body;

  //load users from file
  const encUsers = fileHelpers.readUsersFromFile();

  // Check if username exists
  const user = encUsers.find(user => user.username === username);

  if (!user) {
    errors.username = 'username not found';
    return res.status(400).json(errors);
  }

  const decUser = encryptionHelpers.decryptUser(user);

  if (password !== decUser.password) {
    errors.password = 'Incorrect password';
    return res.status(400).json(errors);
  }

  return res.status(200).json({ success: true });
});

module.exports = router;
