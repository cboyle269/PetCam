const inquirer = require('inquirer');
const encrpytionHelpers = require('./encryptionHelpers');
const fileHelpers = require('./fileHelpers');

const enterSecret = {
  type: 'input',
  name: 'secret',
  message: 'Enter secret to encrypt password'
};

const enterStreamPassword = {
  type: 'input',
  name: 'password',
  message: 'Enter Stream password'
};

const getPassword = async () => {
  const secret = await inquirer.prompt(enterSecret).then(ans => ans['secret']);

  const password = await inquirer
    .prompt(enterStreamPassword)
    .then(ans => ans['password']);

  return {
    secret,
    password
  };
};

getPassword().then(input => {
  const { secret, password } = input;

  const encPass = encrpytionHelpers.encryptStreamPassword(password, secret);

  const streamPass = {
    streamPassword: encPass
  };

  fileHelpers.writeStreamPasswordToFile(streamPass);
});
