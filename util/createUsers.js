const fs = require('fs');
const inquirer = require('inquirer');
const encryptionHelpers = require('./encryptionHelpers');
const fileHelpers = require('./fileHelpers');
const path = require('path');

const questions = [
  {
    type: 'input',
    name: 'username',
    message: 'Enter username'
  },
  {
    type: 'input',
    name: 'password',
    message: 'Enter password'
  }
];

const enterAgain = {
  type: 'confirm',
  name: 'confirmed',
  message: 'Enter another user?',
  default: false
};

const enterSecret = {
  type: 'input',
  name: 'secret',
  message: 'Enter secret to encrypt passwords'
};

const getUsers = async () => {
  let users = [];

  const secret = await inquirer.prompt(enterSecret).then(ans => {
    return ans['secret'];
  });

  let another = true;
  while (another) {
    user = await inquirer.prompt(questions).then(ans => ans);

    users.push(user);

    another = await inquirer.prompt(enterAgain).then(ans => ans['confirmed']);
  }

  return {
    secret,
    users
  };
};

const printUsers = users => {
  [...users].forEach(user => {
    console.log('-------------------------------');
    console.log(`Username: ${user.username}`);
    console.log(`Password: ${user.password}`);
    console.log('-------------------------------');
  });
};

getUsers().then(results => {
  const secret = results.secret;

  let users = results.users;

  const encUsers = encryptionHelpers.encryptUsers(users, secret);

  printUsers(encUsers);

  fileHelpers.writeUsersToFile(encUsers);
});
