const CryptoJS = require('crypto-js');
const config = require('../config/keys');
const fileHelpers = require('./fileHelpers');

module.exports.decryptUser = user => {
  const decPass = CryptoJS.AES.decrypt(user.password, config.secret).toString(
    CryptoJS.enc.Utf8
  );
  user.password = decPass;
  return user;
};

module.exports.encryptUsers = (users, secret) => {
  const encUsers = [...users].map(user => {
    const encPass = CryptoJS.AES.encrypt(user.password, secret).toString();
    user.password = encPass;
    return user;
  });
  return encUsers;
};

module.exports.encryptStreamPassword = (password, secret) => {
  return CryptoJS.AES.encrypt(password, secret).toString();
};

module.exports.decryptStreamPassword = () => {
  const encPass = fileHelpers.readPasswordFromFile();
  return CryptoJS.AES.decrypt(encPass, config.secret).toString(
    CryptoJS.enc.Utf8
  );
};
