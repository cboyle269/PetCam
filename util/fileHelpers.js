const fs = require('fs');
const path = require('path');

const usersFilepath = path.resolve(__dirname, '..', 'data', 'users-enc.json');
const passwordFilepath = path.resolve(
  __dirname,
  '..',
  'data',
  'streampass-enc.json'
);

module.exports.readUsersFromFile = () => {
  const rawData = fs.readFileSync(usersFilepath, 'utf-8');
  const json = JSON.parse(rawData);
  return json;
};

module.exports.writeUsersToFile = users => {
  const data = JSON.stringify(users);
  fs.writeFileSync(usersFilepath, data);
  console.log(`Users were successfully written to ${usersFilepath}`);
};

module.exports.writeStreamPasswordToFile = streamPass => {
  const data = JSON.stringify(streamPass);
  fs.writeFileSync(passwordFilepath, data);
  console.log(`Password successfully written to ${passwordFilepath}`);
};

module.exports.readPasswordFromFile = () => {
  const rawData = fs.readFileSync(passwordFilepath, 'utf-8');
  const json = JSON.parse(rawData);
  return json.streamPassword;
};
