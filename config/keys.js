if (process.env.NODE_ENV === 'production') {
  module.exports.secret = require('./keys_prod').secret;
} else {
  module.exports.secret = require('./keys_dev').secret;
}
